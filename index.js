class Taschenrechner{

    constructor(){

        this.zahl1;
        this.zahl2;   
    }

    addieren(){

        return parseFloat(this.zahl1) + parseFloat(this.zahl2);

    }
    subtrahieren(){

        return parseFloat(this.zahl1) - parseFloat(this.zahl2);
    }

    multiplizieren(){

        return parseFloat(this.zahl1) * parseFloat(this.zahl2);
    }

    dividieren(){

        return parseFloat(this.zahl1) / parseFloat(this.zahl2);
    }
    potenzieren(){

        return Math.pow(parseFloat(this.zahl1), parseFloat(this.zahl2));
    }
    umfangberechnen(){

        return Math.PI * parseFloat(this.zahl1) * 2;
    }
} 

let taschenrechner = new Taschenrechner();

function btnAddierenOnClick(){

    taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value
    
    document.getElementById("lblAusgabe").innerHTML = taschenrechner.addieren();
    
    console.log(taschenrechner);
}
function btnSubtrahierenOnClick(){

   taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value
    
    document.getElementById("lblAusgabe").innerHTML = taschenrechner.subtrahieren();
    
    console.log(taschenrechner);
}
function btnMultiplizierenOnClick(){

   taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value

    document.getElementById("lblAusgabe").innerHTML = taschenrechner.multiplizieren();

    console.log(taschenrechner);
}
function btnDividierenOnClick(){

   taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value

    document.getElementById("lblAusgabe").innerHTML = taschenrechner.dividieren();
    console.log(taschenrechner);
}
function btnPotenzierenOnClick(){

   taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value

    document.getElementById("lblAusgabe").innerHTML = taschenrechner.potenzieren();
    console.log(taschenrechner);
}
function btnUmfangberechnenOnClick(){

    taschenrechner.zahl1 = document.getElementById('tbxZahl1').value
    taschenrechner.zahl2 = document.getElementById('tbxZahl2').value
    
    document.getElementById("lblAusgabe").innerHTML = taschenrechner.umfangberechnen();
    
    console.log(taschenrechner);
}
function getPi(){
    return Math.PI
}
getPi()
if(false){
   
    var sql = require('./sql.js');
    
    var db = new sql.Database();

    db.run("CREATE TABLE test (col1 int, col2 char);");

    db.run("INSERT INTO test VALUES (11, 'Hello')");
    db.run("INSERT INTO test VALUES (12, 'World!')");

    var stmt = db.prepare("SELECT * FROM test");

    var result = stmt.getAsObject({':aval' : 1, ':bval' : 'world'});
    console.log(result);

    stmt.bind({$start:1, $end:2});
    while(stmt.step()) { 
        var row = stmt.getAsObject();

        console.log(row);        
    }
}

