# ReferenzAppJavascript #

**Diese kleine ReferenzApp soll auf einfache Weise grundlegende Konstrukte der zukünftigen Programmierung im Unterricht zeigen:**

* Eingabe über eine Textbox
* Verarbeitung in einer Instanz-Methode
* Ausgabe auf einem Label infolge des Drückens einer btnOnClick()-Function
* Zugriff auf eine Datenbank

Die ReferenzApp basiert auf Javascript und [Electron](http://electron.atom.io).

Sie können Sie ReferenzApp kopieren und je nach Sachverhalt zur Grundlage anderer Apps machen. 

Ändern Sie folgende Dateien **NICHT**:

- `package.json`
- `main.js`

Folgende Dateien sind zur eigenen Bearbeitung vorgesehen:

- `index.html` - Einfügen von Steuererlementen auf der Oberfläche.
- `index.js` - Logik.
- `index.css` - Gestaltung des Aussehens der Steuerelemente.

## Um das Projekt **erstmalig** zu verwenden müssen Sie:

```bash
# Klonen ...
git clone https://bitbucket.org/stbaeumer/referenzappjavascript.git
# Das Verzeichnis betreten ...
cd referenzappjavascript
# Abhängigkeiten installieren ...
npm install
# Starten ...
npm start
```
## Anpassungen der Einstellungen

Sie müssen die Standard-Einstellungen der Schule überschreiben, um Ihre App in der
Schule starten zu können. 

Gehen Sie auf **Datei>Einstellungen>Einstellungen**. Es öffnet sich eine Textdatei namens **settings.json**. Die Datei muss wie folgt angepasst werden:

```bash
// Platzieren Sie Ihre Einstellungen in dieser Datei, um die Standardeinstellungen zu überschreiben.
{
    "update.channel": "none",
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\git-bash.exe",
    "telemetry.enableCrashReporter": false,
    "telemetry.enableTelemetry": false,
    "workbench.colorTheme": "Default Light+"    
}
```


## Um Ihr Projekt zu debuggen, müssen Sie es starten:

```bash
# Das Projekt starten.
npm start
```

Beachten Sie, dass ein einmal gestartetes Projekt auch ohne Neustart bearbeitet werden kann. Der Tastendruck **F5** hat nun (wie in jedem Browser) die Wirkungsweise, dass Veränderungen geladen werden.